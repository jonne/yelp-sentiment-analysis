import pandas as pd
from helpers import load_pickle

print('Experiments 1 & 2')
print('Loading pickle...')
exp_results = load_pickle('results/experiment_1_2_reviews.pkl')
training_set_sizes = [N for N, M in exp_results.keys()]
minibatch_sizes = [M for N, M in exp_results.keys()]
accuracies = [exp_results[(N,M)] for N,M 
                                 in zip(training_set_sizes, 
                                        minibatch_sizes)]

print('Done. Generating data frame...')
data = pd.DataFrame()
data['training_set_size'] = training_set_sizes
data['minibatch_size'] = minibatch_sizes
data['accuracy'] = accuracies

print("Done... Saving to CSV")
data.to_csv("results/exp1_2_results.csv", index=False)

print()
print('Experiment 3')
print('Loading pickle...')
exp3_results = load_pickle('results/experiment_3_reviews.pkl')
l2_penalties = [l2 for l2, _ in exp3_results.items()]
accuracies = [acc for _, acc in exp3_results.items()]
print('Done. Generating data frame...')
data_exp3 = pd.DataFrame()
data_exp3['l2_penalty'] = l2_penalties
data_exp3['accuracy'] = accuracies
print("Done... Saving to CSV")
data_exp3.to_csv("results/exp3_results.csv", index=False)
