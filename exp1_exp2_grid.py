from __future__ import division
from corpus import Document, NamesCorpus, ReviewCorpus, BagOfWords, Name
from maxent import MaxEnt
from unittest import TestCase, main, skip
from random import shuffle, seed
import itertools as it
import multiprocessing
import helpers as h
import pickle
import sys

try:
    corpus_name = sys.argv[1]
except IndexError:
    raise IndexError('You need to provide a corpus name! (names, reviews)')

EXPERIMENT_1_2_PATH = f'./results/experiment_1_2_{corpus_name}.pkl'
training_set_sizes = [1000, 10000, 50000, 100000]
minibatch_sizes = [1, 10, 50, 100, 1000]
l2_penalties = [0.1, 0.5, 1, 10]

def split_names_corpus(document_class=Name, training_set_size=10000):
    """Split the names corpus into training, dev, and test sets"""
    names = NamesCorpus(document_class=document_class)
    if training_set_size == 'all':
        training_set_size = len(names.documents)
        skip_dev = True
        test_is_train = True
    else:
        skip_dev = False
        test_is_train = False
    seed(hash("names"))
    shuffle(names)
    a = training_set_size
    b = a + 1000
    train = names[:a]
    if skip_dev:
        dev = None
    else:
        dev = names[a:b]
    if test_is_train:
        test = train
    else:
        test = names[b:]
    return train, dev, test

def split_review_corpus(document_class=BagOfWords, training_set_size=10000):
    """Split the yelp review corpus into training, dev, and test sets"""
    reviews = ReviewCorpus('yelp_reviews.json', document_class=document_class)
    if training_set_size == 'all':
        training_set_size = len(reviews.documents)
        skip_dev = True
        test_is_train = True
    else:
        skip_dev = False
        test_is_train = False

    seed(hash("reviews"))
    shuffle(reviews)

    a = training_set_size
    b = a + 1000
    c = b + 3000

    train = reviews[:a]
    if skip_dev:
        dev = None
    else:
        dev = reviews[a:b]
    if test_is_train:
        test = train
    else:
        test = reviews[b:c]
    return train, dev, test

def accuracy(classifier, test, verbose=sys.stderr):
    """
    NB: this has been modified from the original started code
        since I needed to vectorize the classification behavior
    """
    predictions = classifier.classify(test)
    correct = [pred == d.label for pred, d in zip(predictions, test)]
    if verbose:
        print("%.2d%% " % (100 * sum(correct) / len(correct)), file=verbose)
    return float(sum(correct)) / len(correct)

# EXPERIMENTS 1 and 2

def experiment(t):
    N, M = t
    #print(f'Experiment #{i}')
    print(f'Training set size: {N}')
    print(f'Minibatch size: {M}')
    if corpus_name == 'names':
        split_corpus = split_names_corpus
    elif corpus_name == 'reviews':
        split_corpus = split_review_corpus
    else:
        raise ValueError('Corpus name must be one of ["names", "reviews"]')

    print('Instantiating classifier...')
    clf = MaxEnt(minibatch_size=M,
                 experiment_name=f"Exp1Exp2_{corpus_name}_N{N}_M{M}")

    print('Done... Splitting corpus...')
    train, dev, test = split_corpus(training_set_size=N)

    print('Done... Performing training...')
    clf.train(train, dev)

    print('Done... Calculting accuracy on dev set...')
    #accuracies[(N,M)] = 
    return accuracy(clf, dev)

experimental_conditions = list(it.product(training_set_sizes, minibatch_sizes))
i = 1

pool = multiprocessing.Pool(7)
accs = pool.map(experiment, experimental_conditions)
accuracies = {t: acc for t, acc in zip(experimental_conditions, accs)}
#accuracies = {}
#for N, M in experimental_conditions:
#    accuracies[(N,M)] = experiment((N,M))
#    i += 1

print('\nExperiments done...\n')
print('Pickling accuracies...')
h.dump_pickle(accuracies, EXPERIMENT_1_2_PATH)
print('All done!')
