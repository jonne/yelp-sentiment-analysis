import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import os

sns.set(style='darkgrid')
sns.set_palette('Set1')

# Constants
BASE_PATH = './results/'
LINESTYLE='solid'

# We can't save where a folder doesn't exist
if not os.path.exists(BASE_PATH):
    os.system(f'mkdir -p {BASE_PATH}')

# We need data to plot
print('Experiment 1 and 2 plots')
data = pd.read_csv(BASE_PATH + 'exp1_2_results.csv')

# Training set vs accuracy
sns.relplot(x="training_set_size", 
           y="accuracy", 
           col_wrap=5, 
           col='minibatch_size',
           kind='line',
           data=data) 
plt.savefig(BASE_PATH + 'training_set_size_vs_accuracy.png')
plt.close()

# Minibatch size vs accuracy
sns.relplot(x="minibatch_size", 
           y="accuracy", 
           col_wrap=4, 
           col="training_set_size",
           kind='line',
           data=data)
plt.savefig(BASE_PATH + 'minibatch_size_vs_accuracy.png')
plt.close()

# Heatmap
heatmap = data.set_index(['training_set_size', 
                          'minibatch_size'])\
              .unstack()
heatmap_numpy = heatmap.to_numpy()
new_heatmap = pd.DataFrame(heatmap_numpy, 
                           columns=data.minibatch_size[:5], 
                           index=np.unique(data.training_set_size))
plt.figure(figsize=(8,8))
ax = sns.heatmap(new_heatmap, cbar=True,
                 vmin=.25, vmax=.75, center=0.5,
                 linewidths=0.5,
                 annot=True, cmap='coolwarm')
ymin, ymax = ax.get_ylim()
ax.set_ylim(ymin+.5, ymax-.5)
plt.yticks(rotation=0) 
plt.ylabel('Training set size')
plt.xlabel('Minibatch size')
plt.suptitle('Accuracy as a function of minibatch and training set sizes')
plt.savefig(BASE_PATH + 'heatmap.png')
plt.close()

print('Experiment 3 plot')

exp3_data = pd.read_csv(BASE_PATH + 'exp3_results.csv')
ax = sns.relplot(x="l2_penalty", 
                 y="accuracy", 
                 kind='line',
                 data=exp3_data) 
plt.ylabel('Accuracy')
plt.xlabel('L2 Penalty')
plt.savefig(BASE_PATH + 'l2_penalty_vs_accuracy.png')
plt.close()
